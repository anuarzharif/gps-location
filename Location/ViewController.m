//
//  ViewController.m
//  Location
//
//  Created by Mohd Zharif Anuar on 12/17/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

@end

@implementation ViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setVariable];
    [self setGraphic];
}

-(void)setVariable
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    geocoder = [[CLGeocoder alloc] init];
}

-(void)setGraphic
{
    self.title = @"Core Location";
}

#pragma mark - locationmanager delegate section
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation *currentLocation = locations[0];
    NSLog(@"didUpdateToLocation: %@", locations);
    
    lblLatitude.text = [NSString stringWithFormat:@"%f", manager.location.coordinate.latitude];
    lblLongitude.text = [NSString stringWithFormat:@"%f", manager.location.coordinate.longitude];
    
    [geocoder reverseGeocodeLocation:currentLocation
                   completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error)
    {
        NSLog(@"Found placemarks: %@, error: %@", placemark.addressDictionary, error);
        if (error == nil &&
            [placemarks count] > 0)
        {
            placemark = [placemarks lastObject];
//            lblAddress.text = [NSString stringWithFormat:@"%@", placemark.addressDictionary[@"FormattedAddressLines"][0]];
            lblAddress.text = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",
                               placemark.subThoroughfare,
                               placemark.thoroughfare,
                               placemark.postalCode,
                               placemark.locality,
                               placemark.administrativeArea,
                               placemark.country];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                           message:[NSString stringWithFormat:@"%@", error.debugDescription]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"Ok"
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil];
            [alert addAction:btnOk];
            [self presentViewController:alert animated:true completion:nil];
        }
    }];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:@"Failed to Get Your Location"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"Ok"
                                                    style:UIAlertActionStyleDefault
                                                  handler:nil];
    [alert addAction:btnOk];
    [self presentViewController:alert animated:true completion:nil];
}

#pragma mark - button section
-(IBAction)btnTapped:(id)sender
{
    [locationManager startUpdatingLocation];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
