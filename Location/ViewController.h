//
//  ViewController.h
//  Location
//
//  Created by Mohd Zharif Anuar on 12/17/16.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController<CLLocationManagerDelegate>
{
    IBOutlet UILabel *lblLatitude;
    IBOutlet UILabel *lblLongitude;
    IBOutlet UILabel *lblAddress;
}

@end

